"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var fs = require('fs');
var SimpleFileDatabase = /** @class */ (function () {
    function SimpleFileDatabase(_a) {
        var path = _a.path;
        this.path = path;
    }
    SimpleFileDatabase.prototype.save = function (data) {
        var _this = this;
        var db = { rules: {}, conditions: {}, values: {} };
        Object.values(data.rules).forEach(function (rule) { return db.rules[rule.name] = _this.ruleSerialisers.default(rule); });
        Object.values(data.conditions).forEach(function (cond) { return db.conditions[cond.name] = _this.conditionSerialisers[cond.type](cond); });
        Object.values(data.values).forEach(function (val) { return db.values[val.name] = _this.valueSerialisers[val.type](val); });
        return new Promise(function (resolve, reject) {
            fs.writeFile(_this.path, JSON.stringify(db), function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    SimpleFileDatabase.prototype.load = function (store) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!fs.existsSync(_this.path))
                fs.writeFileSync(_this.path, "{\"rules\":{}, \"conditions\":{}, \"values\":{}}");
            fs.readFile(_this.path, function (err, dataStr) {
                if (!err && !dataStr)
                    err = new Error("Failed to load data from path '" + _this.path + "'");
                if (!err) {
                    var data = JSON.parse(dataStr);
                    Object.values(data.rules).forEach(function (rule) { return store.putRule(_this.ruleDeserialisers.default(rule)); });
                    Object.values(data.conditions).forEach(function (cond) { return store.putCondition(_this.conditionDeserialisers[cond.type](cond)); });
                    Object.values(data.values).forEach(function (val) { return store.putValue(_this.valueDeserialisers[val.type](val)); });
                    resolve();
                }
                else {
                    reject(err);
                }
            });
        });
    };
    SimpleFileDatabase.prototype.beginWriteback = function (data) {
        // Does nothing - relies on endWriteback to write the whole file in one go.
    };
    SimpleFileDatabase.prototype.endWriteback = function (data) {
        return this.save(data);
    };
    SimpleFileDatabase.prototype.deleteRule = function (name) {
        // Does nothing - relies on endWriteback to write the whole file in one go.
    };
    SimpleFileDatabase.prototype.deleteCondition = function (name) {
        // Does nothing - relies on endWriteback to write the whole file in one go.
    };
    SimpleFileDatabase.prototype.deleteValue = function (name) {
        // Does nothing - relies on endWriteback to write the whole file in one go.
    };
    SimpleFileDatabase.prototype.put = function (data) {
        // Does nothing - relies on endWriteback to write the whole file in one go.
    };
    __decorate([
        blackbox_ioc_1.autowiredService('rule-deserialiser')
    ], SimpleFileDatabase.prototype, "ruleDeserialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowiredService('condition-deserialiser')
    ], SimpleFileDatabase.prototype, "conditionDeserialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowiredService('value-deserialiser')
    ], SimpleFileDatabase.prototype, "valueDeserialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowiredService('rule-serialiser')
    ], SimpleFileDatabase.prototype, "ruleSerialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowiredService('condition-serialiser')
    ], SimpleFileDatabase.prototype, "conditionSerialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowiredService('value-serialiser')
    ], SimpleFileDatabase.prototype, "valueSerialisers", void 0);
    return SimpleFileDatabase;
}());
exports.default = SimpleFileDatabase;
