import { Rule } from "blackbox-rules";
export default class RuleSerialiser {
    conditionDeserialisers: any;
    deserialise(data: any): Rule;
    serialise(rule: Rule): any;
}
