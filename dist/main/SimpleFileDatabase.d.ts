import Database, { Data } from "./Database";
import { Store } from "blackbox-rules";
export default class SimpleFileDatabase implements Database {
    path: string;
    ruleDeserialisers: any;
    conditionDeserialisers: any;
    valueDeserialisers: any;
    ruleSerialisers: any;
    conditionSerialisers: any;
    valueSerialisers: any;
    constructor({ path }: {
        path: any;
    });
    save(data: any): Promise<void>;
    load(store: Store): Promise<void>;
    beginWriteback(data: Data): void;
    endWriteback(data: Data): Promise<void>;
    deleteRule(name: string): void;
    deleteCondition(name: string): void;
    deleteValue(name: string): void;
    put(data: Data): void;
}
