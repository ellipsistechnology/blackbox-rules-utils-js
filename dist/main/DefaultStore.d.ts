import { Condition, Rule, Value, Store } from 'blackbox-rules';
import Database, { Data } from './Database';
export default class DefaultStore implements Store, Data {
    conditions: {
        [key: string]: Condition;
    };
    rules: {
        [key: string]: Rule;
    };
    values: {
        [key: string]: Value<any>;
    };
    database: Database;
    private writebackDelay;
    private maxWritebackDelay;
    private writebackCount;
    private previousWriteTime;
    private putData;
    private deleteData;
    constructor();
    writeback(): void;
    getValue(name: string): Value<any>;
    putValue(value: Value<any>): void;
    removeValue(name: string): void;
    getCondition(name: string): Condition;
    putCondition(cond: Condition): void;
    removeCondition(name: string): void;
    getRule(name: string): Rule;
    putRule(rule: Rule): void;
    removeRule(name: string): void;
    allConditions(): Condition[];
    allRules(): Rule[];
    allValues(): Value<any>[];
}
