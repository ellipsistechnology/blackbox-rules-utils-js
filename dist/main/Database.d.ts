import { Rule, Condition, Value, Store } from "blackbox-rules";
export interface Data {
    rules: {
        [key: string]: Rule;
    };
    conditions: {
        [key: string]: Condition;
    };
    values: {
        [key: string]: Value<any>;
    };
}
export interface DeleteData {
    rules: string[];
    conditions: string[];
    values: string[];
}
export default interface Database {
    load(store: Store): Promise<void>;
    save(data: Data): Promise<void>;
    put(data: Data): any;
    deleteRule(name: string): any;
    deleteCondition(name: string): any;
    deleteValue(name: string): any;
    beginWriteback(data: Data): any;
    endWriteback(data: Data): Promise<void>;
}
