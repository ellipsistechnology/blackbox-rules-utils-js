"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DefaultVariableStore = /** @class */ (function () {
    function DefaultVariableStore() {
        this.vars = {};
    }
    DefaultVariableStore.prototype.put = function (name, v) {
        this.vars[name] = v;
    };
    DefaultVariableStore.prototype.get = function (name) {
        return this.vars[name];
    };
    return DefaultVariableStore;
}());
exports.default = DefaultVariableStore;
