"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_1 = require("blackbox-rules");
var ValueSerialisers = /** @class */ (function () {
    function ValueSerialisers() {
    }
    ValueSerialisers.prototype.deserialiseDefault = function (_data) {
        throw new Error("Value has no default deserialiser.");
    };
    ValueSerialisers.prototype.serialiseDefault = function (_condition) {
        throw new Error("Value has no default serialiser.");
    };
    ValueSerialisers.prototype.deserialiseConstant = function (data) {
        var value = new blackbox_rules_1.ConstantValue(data.val);
        value.name = data.name;
        return value;
    };
    ValueSerialisers.prototype.serialiseConstant = function (value) {
        return value;
    };
    ValueSerialisers.prototype.deserialiseDateTime = function (data) {
        var value = new blackbox_rules_1.DateTimeValue();
        value.name = data.name;
        return value;
    };
    ValueSerialisers.prototype.serialiseDateTime = function (value) {
        return value;
    };
    ValueSerialisers.prototype.deserialiseLog = function (data) {
        var value = new blackbox_rules_1.LogValue();
        value.name = data.name;
        return value;
    };
    ValueSerialisers.prototype.serialiseLog = function (value) {
        return value;
    };
    ValueSerialisers.prototype.deserialiseRemote = function (data) {
        var remoteValueProtocol;
        if (this.remoteValueProtocol)
            remoteValueProtocol = this.remoteValueProtocol;
        else if (this.rulebase && this.rulebase.remoteValueProtocol)
            remoteValueProtocol = this.rulebase.remoteValueProtocol;
        if (!remoteValueProtocol)
            throw new Error('Cannot deserialise remote value without remote value protocol: ' +
                'Remote value protocol not found in either \'remote-value-protocol\' instance' +
                'or in \'rulebase\' instance');
        var value = new blackbox_rules_1.RemoteValue(remoteValueProtocol);
        value.name = data.name;
        value.remoteValueName = data.remoteValueName;
        value.remoteAddress = data.remoteAddress;
        return value;
    };
    ValueSerialisers.prototype.serialiseRemote = function (value) {
        return {
            remoteAddress: value.remoteAddress,
            remoteValueName: value.remoteValueName,
            name: value.name,
            type: value.type
        };
    };
    ValueSerialisers.prototype.deserialiseVariable = function (data) {
        var store;
        if (this.store)
            store = this.store;
        else if (this.rulebase && this.rulebase.store)
            store = this.rulebase.store;
        if (!store)
            throw new Error('Cannot deserialise variable value without store: ' +
                'Store not found in either \'rulebase-store\' instance' +
                'or in \'rulebase\' instance');
        var value = new blackbox_rules_1.VariableValue(store);
        value.name = data.name;
        value.variableName = data.variableName;
        return value;
    };
    ValueSerialisers.prototype.serialiseVariable = function (value) {
        return value;
    };
    __decorate([
        blackbox_ioc_1.autowired('rulebase')
    ], ValueSerialisers.prototype, "rulebase", void 0);
    __decorate([
        blackbox_ioc_1.autowired('rulebase-store')
    ], ValueSerialisers.prototype, "store", void 0);
    __decorate([
        blackbox_ioc_1.autowired('remote-value-protocol')
    ], ValueSerialisers.prototype, "remoteValueProtocol", void 0);
    __decorate([
        blackbox_ioc_1.taggedService('value-deserialiser', 'default')
    ], ValueSerialisers.prototype, "deserialiseDefault", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-serialiser', 'default')
    ], ValueSerialisers.prototype, "serialiseDefault", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-deserialiser', 'constant')
    ], ValueSerialisers.prototype, "deserialiseConstant", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-serialiser', 'constant')
    ], ValueSerialisers.prototype, "serialiseConstant", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-deserialiser', 'date-time')
    ], ValueSerialisers.prototype, "deserialiseDateTime", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-serialiser', 'date-time')
    ], ValueSerialisers.prototype, "serialiseDateTime", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-deserialiser', 'log')
    ], ValueSerialisers.prototype, "deserialiseLog", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-serialiser', 'log')
    ], ValueSerialisers.prototype, "serialiseLog", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-deserialiser', 'remote')
    ], ValueSerialisers.prototype, "deserialiseRemote", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-serialiser', 'remote')
    ], ValueSerialisers.prototype, "serialiseRemote", null);
    __decorate([
        blackbox_ioc_1.taggedService('value-deserialiser', 'variable')
    ], ValueSerialisers.prototype, "deserialiseVariable", null);
    __decorate([
        blackbox_ioc_1.taggedService('variable-serialiser', 'variable')
    ], ValueSerialisers.prototype, "serialiseVariable", null);
    return ValueSerialisers;
}());
exports.default = ValueSerialisers;
