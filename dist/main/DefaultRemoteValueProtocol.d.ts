import { RemoteValueProtocol } from 'blackbox-rules';
export default class DefaultRemoteValueProtocol implements RemoteValueProtocol {
    lookupAgentValueAddress(agentAddress: string): string;
    setValue<T>(agentValueAddress: string, remoteValueName: string, value: T): Promise<T>;
    getValue<T>(agentValueAddress: string, remoteValueName: string): Promise<T>;
}
