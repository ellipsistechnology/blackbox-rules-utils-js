import RuleBase, { Condition } from "blackbox-rules";
/**
 * Provides serialisation and deserialisation serbices for conditions.
 * Services are tagged with condition-serialiser and condition-deserialiser.
 * Note that since conditions are also values, each serialiser is tagged with both
 * condition-serialiser and value-serialiser, and each deserialiser is tagged with
 * both condition-deserialiser and value-deserialiser
 */
export default class ConditionSerialisers {
    valueDeserialisers: any;
    rulebase: RuleBase;
    deserialise(data: any, constructor: (props: any) => Condition): Condition;
    serialise(condition: Condition): any;
    deserialiseDefault(data: any): Condition;
    serialiseDefault(condition: Condition): any;
    deserialiseEquals(data: any): Condition;
    serialiseEquals(condition: Condition): any;
    deserialiseLessThan(data: any): Condition;
    serialiseLessThan(condition: Condition): any;
    deserialiseGreaterThan(data: any): Condition;
    serialiseGreaterThan(condition: Condition): any;
    deserialiseAnd(data: any): Condition;
    serialiseAnd(condition: Condition): any;
    deserialiseOr(data: any): Condition;
    serialiseOr(condition: Condition): any;
    deserialiseUnconditional(data: any): Condition;
    serialiseUnconditional(condition: Condition): any;
}
