"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var DefaultStore = /** @class */ (function () {
    function DefaultStore() {
        this.conditions = {};
        this.rules = {};
        this.values = {};
        if (this.database) {
            this.writebackDelay = 1000;
            this.maxWritebackDelay = 30000;
            this.writebackCount = 0;
            this.previousWriteTime = 0;
            this.putData = { rules: {}, conditions: {}, values: {} };
            this.deleteData = { rules: [], conditions: [], values: [] };
            this.database.load(this).catch(function (err) {
                if (err)
                    console.error(err);
            });
        }
    }
    DefaultStore.prototype.writeback = function () {
        var _this = this;
        if (this.database) {
            this.writebackCount++;
            var previousCount_1 = this.writebackCount;
            setTimeout(function () {
                var now = new Date().getMilliseconds();
                // Only write back if nothing added recently or if already waited too long:
                if (_this.writebackCount === previousCount_1 || (now - _this.previousWriteTime > _this.maxWritebackDelay)) {
                    _this.previousWriteTime = now;
                    _this.writebackCount = 0;
                    var writebackData = _this.putData;
                    _this.putData = { rules: {}, conditions: {}, values: {} };
                    var deleteData = _this.deleteData;
                    _this.deleteData = { rules: [], conditions: [], values: [] };
                    _this.database.beginWriteback(_this);
                    deleteData.rules.forEach(function (name) { return _this.database.deleteRule(name); });
                    deleteData.conditions.forEach(function (name) { return _this.database.deleteCondition(name); });
                    deleteData.values.forEach(function (name) { return _this.database.deleteValue(name); });
                    if (Object.keys(writebackData).length > 0)
                        _this.database.put(writebackData);
                    _this.database.endWriteback(_this);
                }
            }, this.writebackDelay);
        }
    };
    DefaultStore.prototype.getValue = function (name) {
        return this.values[name];
    };
    DefaultStore.prototype.putValue = function (value) {
        if (this.putData) {
            this.putData.values[value.name] = value;
            this.writeback();
        }
        this.values[value.name] = value;
    };
    DefaultStore.prototype.removeValue = function (name) {
        if (this.deleteData) {
            this.deleteData.values.push(name);
            this.writeback();
        }
        delete this.values[name];
    };
    DefaultStore.prototype.getCondition = function (name) {
        return this.conditions[name];
    };
    DefaultStore.prototype.putCondition = function (cond) {
        if (cond) {
            if (this.putData) {
                this.putData.conditions[cond.name] = cond;
                this.writeback();
            }
            this.conditions[cond.name] = cond;
        }
    };
    DefaultStore.prototype.removeCondition = function (name) {
        if (this.deleteData) {
            this.deleteData.conditions.push(name);
            this.writeback();
        }
        delete this.conditions[name];
    };
    DefaultStore.prototype.getRule = function (name) {
        return this.rules[name];
    };
    DefaultStore.prototype.putRule = function (rule) {
        if (rule) {
            if (this.putData) {
                this.putData.rules[rule.name] = rule;
                this.writeback();
            }
            this.rules[rule.name] = rule;
        }
    };
    DefaultStore.prototype.removeRule = function (name) {
        if (this.deleteData) {
            this.deleteData.rules.push(name);
            this.writeback();
        }
        delete this.rules[name];
    };
    DefaultStore.prototype.allConditions = function () {
        return Object.values(this.conditions);
    };
    DefaultStore.prototype.allRules = function () {
        return Object.values(this.rules);
    };
    DefaultStore.prototype.allValues = function () {
        return Object.values(this.values);
    };
    __decorate([
        blackbox_ioc_1.autowired('rules-database')
    ], DefaultStore.prototype, "database", void 0);
    return DefaultStore;
}());
exports.default = DefaultStore;
