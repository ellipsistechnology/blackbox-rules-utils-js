import RuleBase, { Store, RemoteValueProtocol, VariableStore } from 'blackbox-rules';
export default class DefaultRuleBase extends RuleBase {
    wiredStore: Store;
    wiredRemoteValueProtocol: RemoteValueProtocol;
    wiredVariableStore: VariableStore;
    constructor();
}
