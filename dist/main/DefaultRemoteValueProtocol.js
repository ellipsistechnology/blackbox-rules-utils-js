"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var DefaultRemoteValueProtocol = /** @class */ (function () {
    function DefaultRemoteValueProtocol() {
    }
    DefaultRemoteValueProtocol.prototype.lookupAgentValueAddress = function (agentAddress) {
        return agentAddress + "/values";
    };
    DefaultRemoteValueProtocol.prototype.setValue = function (agentValueAddress, remoteValueName, value) {
        return axios_1.default.patch(agentValueAddress + "/" + remoteValueName, { value: value });
    };
    DefaultRemoteValueProtocol.prototype.getValue = function (agentValueAddress, remoteValueName) {
        return axios_1.default.get(agentValueAddress + "/" + remoteValueName);
    };
    return DefaultRemoteValueProtocol;
}());
exports.default = DefaultRemoteValueProtocol;
