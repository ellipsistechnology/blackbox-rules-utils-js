export default class DefaultVariableStore {
    private vars;
    constructor();
    put(name: string, v: any): void;
    get(name: string): any;
}
