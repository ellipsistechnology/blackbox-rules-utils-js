"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_rules_1 = require("blackbox-rules");
var DefaultStore_1 = require("./DefaultStore");
var DefaultRemoteValueProtocol_1 = require("./DefaultRemoteValueProtocol");
var DefaultVariableStore_1 = require("./DefaultVariableStore");
var blackbox_ioc_1 = require("blackbox-ioc");
var DefaultRuleBase = /** @class */ (function (_super) {
    __extends(DefaultRuleBase, _super);
    function DefaultRuleBase() {
        var _this = _super.call(this, {
            runPeriod: 1000
        }) || this;
        if (_this.wiredRemoteValueProtocol)
            _this.remoteValueProtocol = _this.wiredRemoteValueProtocol;
        else
            _this.remoteValueProtocol = new DefaultRemoteValueProtocol_1.default();
        if (_this.wiredStore)
            _this.store = _this.wiredStore;
        else
            _this.store = new DefaultStore_1.default();
        if (_this.wiredVariableStore)
            _this.variableStore = _this.wiredVariableStore;
        else
            _this.variableStore = new DefaultVariableStore_1.default();
        return _this;
    }
    __decorate([
        blackbox_ioc_1.autowired('rulebase-store')
    ], DefaultRuleBase.prototype, "wiredStore", void 0);
    __decorate([
        blackbox_ioc_1.autowired('remote-value-protocol')
    ], DefaultRuleBase.prototype, "wiredRemoteValueProtocol", void 0);
    __decorate([
        blackbox_ioc_1.autowired('variable-store')
    ], DefaultRuleBase.prototype, "wiredVariableStore", void 0);
    return DefaultRuleBase;
}(blackbox_rules_1.default));
exports.default = DefaultRuleBase;
