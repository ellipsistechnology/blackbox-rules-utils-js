"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_1 = require("blackbox-rules");
/**
 * Provides serialisation and deserialisation serbices for conditions.
 * Services are tagged with condition-serialiser and condition-deserialiser.
 * Note that since conditions are also values, each serialiser is tagged with both
 * condition-serialiser and value-serialiser, and each deserialiser is tagged with
 * both condition-deserialiser and value-deserialiser
 */
var ConditionSerialisers = /** @class */ (function () {
    function ConditionSerialisers() {
    }
    ConditionSerialisers.prototype.deserialise = function (data, constructor) {
        if (!data)
            return data;
        var left = this.valueDeserialisers[data.left.type](data.left);
        var right = this.valueDeserialisers[data.right.type](data.right);
        return constructor({
            name: data.name,
            left: left,
            right: right,
            type: data.type,
            template: data.template
        });
    };
    ConditionSerialisers.prototype.serialise = function (condition) {
        return condition;
    };
    ConditionSerialisers.prototype.deserialiseDefault = function (data) {
        return this.deserialise(data, function (props) { return new blackbox_rules_1.ConditionImpl(props); });
    };
    ConditionSerialisers.prototype.serialiseDefault = function (condition) {
        return this.serialise(condition);
    };
    ConditionSerialisers.prototype.deserialiseEquals = function (data) {
        return this.deserialise(data, function (props) { return new blackbox_rules_1.EqualsCondition(props); });
    };
    ConditionSerialisers.prototype.serialiseEquals = function (condition) {
        return this.serialise(condition);
    };
    ConditionSerialisers.prototype.deserialiseLessThan = function (data) {
        return this.deserialise(data, function (props) { return new blackbox_rules_1.LessThanCondition(props); });
    };
    ConditionSerialisers.prototype.serialiseLessThan = function (condition) {
        return this.serialise(condition);
    };
    ConditionSerialisers.prototype.deserialiseGreaterThan = function (data) {
        return this.deserialise(data, function (props) { return new blackbox_rules_1.GreaterThanCondition(props); });
    };
    ConditionSerialisers.prototype.serialiseGreaterThan = function (condition) {
        return this.serialise(condition);
    };
    ConditionSerialisers.prototype.deserialiseAnd = function (data) {
        return this.deserialise(data, function (props) { return new blackbox_rules_1.AndCondition(props); });
    };
    ConditionSerialisers.prototype.serialiseAnd = function (condition) {
        return this.serialise(condition);
    };
    ConditionSerialisers.prototype.deserialiseOr = function (data) {
        return this.deserialise(data, function (props) { return new blackbox_rules_1.OrCondition(props); });
    };
    ConditionSerialisers.prototype.serialiseOr = function (condition) {
        return this.serialise(condition);
    };
    ConditionSerialisers.prototype.deserialiseUnconditional = function (data) {
        if (!this.rulebase)
            throw new Error("Cannot deserialise an unconditional value without a 'rulebase' instance.");
        var uncond = this.rulebase.unconditional();
        uncond.name = data.name;
        return uncond;
    };
    ConditionSerialisers.prototype.serialiseUnconditional = function (condition) {
        return this.serialise(condition);
    };
    __decorate([
        blackbox_ioc_1.autowiredService('value-deserialiser')
    ], ConditionSerialisers.prototype, "valueDeserialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowired('rulebase')
    ], ConditionSerialisers.prototype, "rulebase", void 0);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', 'default')
    ], ConditionSerialisers.prototype, "deserialiseDefault", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', 'default')
    ], ConditionSerialisers.prototype, "serialiseDefault", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', '='),
        blackbox_ioc_1.taggedService('value-deserialiser', '=')
    ], ConditionSerialisers.prototype, "deserialiseEquals", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', '='),
        blackbox_ioc_1.taggedService('value-serialiser', '=')
    ], ConditionSerialisers.prototype, "serialiseEquals", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', '<'),
        blackbox_ioc_1.taggedService('value-deserialiser', '<')
    ], ConditionSerialisers.prototype, "deserialiseLessThan", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', '<'),
        blackbox_ioc_1.taggedService('value-serialiser', '<')
    ], ConditionSerialisers.prototype, "serialiseLessThan", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', '>'),
        blackbox_ioc_1.taggedService('value-deserialiser', '>')
    ], ConditionSerialisers.prototype, "deserialiseGreaterThan", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', '>'),
        blackbox_ioc_1.taggedService('value-serialiser', '>')
    ], ConditionSerialisers.prototype, "serialiseGreaterThan", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', 'and'),
        blackbox_ioc_1.taggedService('value-deserialiser', 'and')
    ], ConditionSerialisers.prototype, "deserialiseAnd", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', 'and'),
        blackbox_ioc_1.taggedService('value-serialiser', 'and')
    ], ConditionSerialisers.prototype, "serialiseAnd", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', 'or'),
        blackbox_ioc_1.taggedService('value-deserialiser', 'or')
    ], ConditionSerialisers.prototype, "deserialiseOr", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', 'or'),
        blackbox_ioc_1.taggedService('value-serialiser', 'or')
    ], ConditionSerialisers.prototype, "serialiseOr", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-deserialiser', 'unconditional'),
        blackbox_ioc_1.taggedService('value-deserialiser', 'unconditional')
    ], ConditionSerialisers.prototype, "deserialiseUnconditional", null);
    __decorate([
        blackbox_ioc_1.taggedService('condition-serialiser', 'unconditional'),
        blackbox_ioc_1.taggedService('value-serialiser', 'unconditional')
    ], ConditionSerialisers.prototype, "serialiseUnconditional", null);
    return ConditionSerialisers;
}());
exports.default = ConditionSerialisers;
