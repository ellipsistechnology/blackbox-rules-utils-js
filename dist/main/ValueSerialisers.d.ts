import RuleBase, { Value, RemoteValue, RemoteValueProtocol, Store } from "blackbox-rules";
export default class ValueSerialisers {
    rulebase: RuleBase;
    store: Store;
    remoteValueProtocol: RemoteValueProtocol;
    deserialiseDefault(_data: any): Value<any>;
    serialiseDefault(_condition: Value<any>): any;
    deserialiseConstant(data: any): Value<any>;
    serialiseConstant(value: Value<any>): any;
    deserialiseDateTime(data: any): Value<any>;
    serialiseDateTime(value: Value<any>): any;
    deserialiseLog(data: any): Value<any>;
    serialiseLog(value: Value<any>): any;
    deserialiseRemote(data: any): Value<any>;
    serialiseRemote(value: RemoteValue<any>): any;
    deserialiseVariable(data: any): Value<any>;
    serialiseVariable(value: Value<any>): any;
}
