"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_1 = require("blackbox-rules");
var RuleSerialiser = /** @class */ (function () {
    function RuleSerialiser() {
    }
    RuleSerialiser.prototype.deserialise = function (data) {
        if (!data)
            return data;
        var action = this.conditionDeserialisers[data.action.type](data.action);
        var trigger = this.conditionDeserialisers[data.trigger.type](data.trigger);
        var preCondition, postCondition;
        if (data.preCondition)
            preCondition = this.conditionDeserialisers[data.preCondition.type](data.preCondition);
        if (data.postCondition)
            postCondition = this.conditionDeserialisers[data.postCondition.type](data.postCondition);
        return new blackbox_rules_1.Rule({
            name: data.name,
            preCondition: preCondition,
            postCondition: postCondition,
            trigger: trigger,
            action: action
        });
    };
    RuleSerialiser.prototype.serialise = function (rule) {
        return rule;
    };
    __decorate([
        blackbox_ioc_1.autowiredService('condition-deserialiser')
    ], RuleSerialiser.prototype, "conditionDeserialisers", void 0);
    __decorate([
        blackbox_ioc_1.taggedService('rule-deserialiser', 'default')
    ], RuleSerialiser.prototype, "deserialise", null);
    __decorate([
        blackbox_ioc_1.taggedService('rule-serialiser', 'default')
    ], RuleSerialiser.prototype, "serialise", null);
    return RuleSerialiser;
}());
exports.default = RuleSerialiser;
