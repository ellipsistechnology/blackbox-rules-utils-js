"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var DefaultStore_1 = require("../main/DefaultStore");
var blackbox_rules_1 = require("blackbox-rules");
var blackbox_ioc_1 = require("blackbox-ioc");
var SimpleFileDatabase_1 = require("../main/SimpleFileDatabase");
jest.mock('fs');
var RULE_NAME = "Rule 001";
var CONDITION_NAME = "Condition 001";
var VALUE_NAME = "Value 001";
test("Can create DefaultStore", function () {
    var store = new DefaultStore_1.default();
    expect(store.allRules).toBeDefined();
    expect(store.allRules.length).toBe(0);
    expect(store.allConditions).toBeDefined();
    expect(store.allConditions.length).toBe(0);
    expect(store.allValues).toBeDefined();
    expect(store.allValues.length).toBe(0);
});
function testRules() {
    var store = new DefaultStore_1.default();
    var r = new blackbox_rules_1.Rule({ name: RULE_NAME });
    store.putRule(r);
    expect(store.getRule(RULE_NAME)).toBe(r);
    store.removeRule(RULE_NAME);
    expect(store.getRule(RULE_NAME)).toBeUndefined();
}
test("DefaultStore can store, load and delete rules", testRules);
function testConditions() {
    var _this = this;
    var store = new DefaultStore_1.default();
    var c = {
        name: CONDITION_NAME,
        type: 'test',
        checkTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, true];
        }); }); },
        makeTrue: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); }); },
        get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, null];
        }); }); },
        set: function (v) { }
    };
    store.putCondition(c);
    expect(store.getCondition(CONDITION_NAME)).toBe(c);
    store.removeCondition(CONDITION_NAME);
    expect(store.getCondition(CONDITION_NAME)).toBeUndefined();
}
test("DefaultStore can store, load and delete conditions", testConditions);
function testValues() {
    var _this = this;
    var store = new DefaultStore_1.default();
    var v = {
        name: VALUE_NAME,
        type: 'test',
        get: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, null];
        }); }); },
        set: function (v) { }
    };
    store.putValue(v);
    expect(store.getValue(VALUE_NAME)).toBe(v);
    store.removeValue(VALUE_NAME);
    expect(store.getValue(VALUE_NAME)).toBeUndefined();
}
test("DefaultStore can store, load and delete values", testValues);
test("DefaultStore can create, get and delete rules, conditions and values with a database", function () {
    var Factory = /** @class */ (function () {
        function Factory() {
        }
        Factory.prototype.makeDatabase = function () { return new SimpleFileDatabase_1.default({ path: '/' }); };
        __decorate([
            blackbox_ioc_1.factory('rules-database')
        ], Factory.prototype, "makeDatabase", null);
        return Factory;
    }());
    var store = new DefaultStore_1.default();
    expect(store.database).toBeDefined();
    testRules();
    testConditions();
    testValues();
});
