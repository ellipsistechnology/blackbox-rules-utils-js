"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var SimpleFileDatabase_1 = require("../main/SimpleFileDatabase");
var DefaultStore_1 = require("../main/DefaultStore");
var RuleSerialiser_1 = require("../main/RuleSerialiser");
var ConditionSerialisers_1 = require("../main/ConditionSerialisers");
var ValueSerialisers_1 = require("../main/ValueSerialisers");
new RuleSerialiser_1.default();
new ConditionSerialisers_1.default();
new ValueSerialisers_1.default();
jest.mock('fs');
var fs = require('fs');
function makeTestData() {
    return {
        rules: {
            'test rule': {
                name: 'test rule',
                preCondition: {
                    type: '=',
                    name: 'preCondition',
                    left: { name: 'preCondition.left', type: 'constant', val: 'test1' },
                    right: { name: 'preCondition.right', type: 'constant', val: 'test1' }
                },
                trigger: {
                    type: '=',
                    name: 'trigger',
                    left: { name: 'trigger.left', type: 'constant', val: 'test2' },
                    right: { name: 'trigger.right', type: 'constant', val: 'test2' }
                },
                action: {
                    type: '=',
                    name: 'action',
                    left: { name: 'action.left', type: 'constant', val: 'test3' },
                    right: { name: 'action.right', type: 'constant', val: 'test4' }
                },
                postCondition: {
                    type: '=',
                    name: 'postCondition',
                    left: { name: 'postCondition.left', type: 'constant', val: 'test5' },
                    right: { name: 'postCondition.right', type: 'constant', val: 'test6' }
                }
            }
        },
        conditions: {
            preCondition: {
                type: '=',
                name: 'preCondition',
                left: { name: 'preCondition.left', type: 'constant', val: 'test1' },
                right: { name: 'preCondition.right', type: 'constant', val: 'test1' }
            },
            trigger: {
                type: '=',
                name: 'trigger',
                left: { name: 'trigger.left', type: 'constant', val: 'test2' },
                right: { name: 'trigger.right', type: 'constant', val: 'test2' }
            },
            action: {
                type: '=',
                name: 'action',
                left: { name: 'action.left', type: 'constant', val: 'test3' },
                right: { name: 'action.right', type: 'constant', val: 'test4' }
            },
            postCondition: {
                type: '=',
                name: 'postCondition',
                left: { name: 'postCondition.left', type: 'constant', val: 'test5' },
                right: { name: 'postCondition.right', type: 'constant', val: 'test6' }
            }
        },
        values: {
            'preCondition.left': { name: 'preCondition.left', type: 'constant', val: 'test1' },
            'preCondition.right': { name: 'preCondition.right', type: 'constant', val: 'test1' },
            'trigger.left': { name: 'trigger.left', type: 'constant', val: 'test2' },
            'trigger.right': { name: 'trigger.right', type: 'constant', val: 'test2' },
            'action.left': { name: 'action.left', type: 'constant', val: 'test3' },
            'action.right': { name: 'action.right', type: 'constant', val: 'test4' },
            'postCondition.left': { name: 'postCondition.left', type: 'constant', val: 'test5' },
            'postCondition.right': { name: 'postCondition.right', type: 'constant', val: 'test6' }
        }
    };
}
test("Can save and load data", function () { return __awaiter(_this, void 0, void 0, function () {
    var database, store, testData;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                database = new SimpleFileDatabase_1.default({ path: '/test.json' });
                store = new DefaultStore_1.default();
                testData = makeTestData();
                return [4 /*yield*/, database.save(testData)];
            case 1:
                _a.sent();
                return [4 /*yield*/, database.load(store)];
            case 2:
                _a.sent();
                expect(store.getRule('test rule')).toEqual(testData.rules['test rule']);
                expect(store.getCondition('action')).toEqual(testData.conditions['action']);
                expect(store.getValue('trigger.right')).toEqual(testData.values['trigger.right']);
                return [2 /*return*/];
        }
    });
}); });
test("Can add rules, conditions and values in a writeback transaction.", function () { return __awaiter(_this, void 0, void 0, function () {
    var database, store, testData, data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                database = new SimpleFileDatabase_1.default({ path: '/test.json' });
                store = new DefaultStore_1.default();
                testData = makeTestData();
                return [4 /*yield*/, database.save(testData)];
            case 1:
                _a.sent();
                database.beginWriteback(store);
                store.putRule({
                    name: 'test rule 2',
                    preCondition: {
                        type: '=',
                        name: 'preCondition',
                        left: { name: 'preCondition.left', type: 'constant', val: 'test1' },
                        right: { name: 'preCondition.right', type: 'constant', val: 'test1' }
                    },
                    trigger: {
                        type: '=',
                        name: 'trigger',
                        left: { name: 'trigger.left', type: 'constant', val: 'test2' },
                        right: { name: 'trigger.right', type: 'constant', val: 'test2' }
                    },
                    action: {
                        type: '=',
                        name: 'action',
                        left: { name: 'action.left', type: 'constant', val: 'test3' },
                        right: { name: 'action.right', type: 'constant', val: 'test4' }
                    },
                    postCondition: {
                        type: '=',
                        name: 'postCondition',
                        left: { name: 'postCondition.left', type: 'constant', val: 'test5' },
                        right: { name: 'postCondition.right', type: 'constant', val: 'test6' }
                    }
                });
                store.putCondition({
                    type: '=',
                    name: 'preCondition2',
                    left: { name: 'preCondition.left', type: 'constant', val: 'test1' },
                    right: { name: 'preCondition.right', type: 'constant', val: 'test1' }
                });
                store.putValue({ name: 'preCondition.right2', type: 'constant', val: 'test1' });
                return [4 /*yield*/, database.endWriteback(store)];
            case 2:
                _a.sent();
                data = JSON.parse(fs.__fileMap()['/test.json']);
                expect(data.rules['test rule 2']).toBeDefined();
                expect(data.conditions['preCondition2']).toBeDefined();
                expect(data.values['preCondition.right2']).toBeDefined();
                return [2 /*return*/];
        }
    });
}); });
test("Can delete rules, conditions and values in a writeback transaction.", function () { return __awaiter(_this, void 0, void 0, function () {
    var database, store, testData, data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                database = new SimpleFileDatabase_1.default({ path: '/test.json' });
                store = new DefaultStore_1.default();
                testData = makeTestData();
                return [4 /*yield*/, database.save(testData)];
            case 1:
                _a.sent();
                database.beginWriteback(store);
                store.removeRule('testRule');
                store.removeCondition('testCondition');
                store.removeValue('testValue');
                database.deleteRule('testRule');
                database.deleteCondition('testCondition');
                database.deleteValue('testValue');
                return [4 /*yield*/, database.endWriteback(store)];
            case 2:
                _a.sent();
                data = JSON.parse(fs.__fileMap()['/test.json']);
                expect(data.rules.testRule).toBeUndefined();
                expect(data.conditions.testCondition).toBeUndefined();
                expect(data.values.testValue).toBeUndefined();
                return [2 /*return*/];
        }
    });
}); });
