"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DefaultVariableStore_1 = require("../main/DefaultVariableStore");
var TEST_NAME1 = 'test 1';
var TEST_NAME2 = 'test 2';
var TEST_NAME3 = 'test 3';
var TEST_VALUE1 = 'string';
var TEST_VALUE2 = 1;
var TEST_VALUE3 = { thing: 'string' };
test("Can create DefaultVariableStore", function () {
    var store = new DefaultVariableStore_1.default();
    store.put(TEST_NAME1, TEST_VALUE1);
    store.put(TEST_NAME2, TEST_VALUE2);
    store.put(TEST_NAME3, TEST_VALUE3);
    expect(store.get(TEST_NAME1)).toBe(TEST_VALUE1);
    expect(store.get(TEST_NAME2)).toBe(TEST_VALUE2);
    expect(store.get(TEST_NAME3)).toBe(TEST_VALUE3);
});
