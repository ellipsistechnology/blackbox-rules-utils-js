"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_1 = require("blackbox-rules");
var ConditionSerialisers_1 = require("../main/ConditionSerialisers");
var ValueSerialisers_1 = require("../main/ValueSerialisers");
var DefaultRuleBase_1 = require("../main/DefaultRuleBase");
// Force loading of services:
new ConditionSerialisers_1.default();
new ValueSerialisers_1.default();
var Wrapper = /** @class */ (function () {
    function Wrapper() {
    }
    Wrapper.prototype.rulebase = function () { return new DefaultRuleBase_1.default(); };
    __decorate([
        blackbox_ioc_1.autowiredService('condition-serialiser')
    ], Wrapper.prototype, "serialisers", void 0);
    __decorate([
        blackbox_ioc_1.autowiredService('condition-deserialiser')
    ], Wrapper.prototype, "deserialisers", void 0);
    __decorate([
        blackbox_ioc_1.factory('rulebase')
    ], Wrapper.prototype, "rulebase", null);
    return Wrapper;
}());
test("Can deserialise a default condition", function () {
    var wrapper = new Wrapper();
    var data = {
        name: "test condition",
        left: {
            name: 'test',
            type: 'constant',
            val: { thing: 'test value' }
        },
        right: {
            name: 'test',
            type: 'constant',
            val: { thing: 'test value' }
        },
        type: "x",
        template: {
            name: "test template"
        }
    };
    var deserialised = wrapper.deserialisers['default'](data);
    expect(deserialised).toBeInstanceOf(blackbox_rules_1.ConditionImpl);
    expect(deserialised.left).toBeInstanceOf(blackbox_rules_1.ConstantValue);
    expect(deserialised.right).toBeInstanceOf(blackbox_rules_1.ConstantValue);
    expect(deserialised.name).toEqual(data.name);
    expect(deserialised.type).toEqual(data.type);
    expect(deserialised.template).toEqual(data.template);
});
function testCondition(type, Type) {
    var wrapper = new Wrapper();
    var data = {
        name: "test condition",
        left: {
            name: 'test',
            type: 'constant',
            val: { thing: 'test value' }
        },
        right: {
            name: 'test',
            type: 'constant',
            val: { thing: 'test value' }
        },
        type: type,
        template: {
            name: "test template"
        }
    };
    var deserialised = wrapper.deserialisers[type](data);
    expect(deserialised).toBeInstanceOf(Type);
    expect(deserialised.left).toBeInstanceOf(blackbox_rules_1.ConstantValue);
    expect(deserialised.right).toBeInstanceOf(blackbox_rules_1.ConstantValue);
    expect(deserialised.name).toEqual(data.name);
    expect(deserialised.type).toEqual(data.type);
    expect(deserialised.template).toEqual(data.template);
    expect(deserialised.checkTrue()).toBeTruthy();
}
test("Can deserialise an equals condition", function () {
    testCondition("=", blackbox_rules_1.EqualsCondition);
});
test("Can deserialise a less then condition", function () {
    testCondition("<", blackbox_rules_1.LessThanCondition);
});
test("Can deserialise a greater than condition", function () {
    testCondition(">", blackbox_rules_1.GreaterThanCondition);
});
function testAndOr(type) {
    var wrapper = new Wrapper();
    var data = {
        name: "test condition",
        left: {
            name: "left",
            type: "=",
            left: {
                name: 'test',
                type: 'constant',
                val: { thing: 'test value' }
            },
            right: {
                name: 'test',
                type: 'constant',
                val: { thing: 'test value' }
            }
        },
        right: {
            name: "right",
            type: "=",
            left: {
                name: 'test',
                type: 'constant',
                val: { thing: 'test value' }
            },
            right: {
                name: 'test',
                type: 'constant',
                val: { thing: 'test value' }
            }
        },
        type: type,
        template: {
            name: "test template"
        }
    };
    var deserialised = wrapper.deserialisers[type](data);
    expect(deserialised).toBeInstanceOf(blackbox_rules_1.ConditionImpl);
    expect(deserialised.left).toBeInstanceOf(blackbox_rules_1.ConditionImpl);
    expect(deserialised.left.name).toEqual(data.left.name);
    expect(deserialised.right).toBeInstanceOf(blackbox_rules_1.ConditionImpl);
    expect(deserialised.right.name).toEqual(data.right.name);
    expect(deserialised.name).toEqual(data.name);
    expect(deserialised.type).toEqual(data.type);
    expect(deserialised.template).toEqual(data.template);
    expect(deserialised.checkTrue()).toBeTruthy();
}
test("Can deserialise an and condition", function () {
    testAndOr("and");
});
test("Can deserialise an or condition", function () {
    testAndOr("or");
});
test("Can deserialise an unconditional", function () {
    var wrapper = new Wrapper();
    var data = {
        name: "test unconditional",
        type: "unconditional"
    };
    var deserialised = wrapper.deserialisers.unconditional(data);
    expect(deserialised.name).toEqual(data.name);
    expect(deserialised.type).toEqual(data.type);
    expect(deserialised.checkTrue()).toBeTruthy();
});
