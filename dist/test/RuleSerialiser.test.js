"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_1 = require("blackbox-rules");
var RuleSerialiser_1 = require("../main/RuleSerialiser");
var ValueSerialisers_1 = require("../main/ValueSerialisers");
var ConditionSerialisers_1 = require("../main/ConditionSerialisers");
new RuleSerialiser_1.default();
new ConditionSerialisers_1.default();
new ValueSerialisers_1.default();
var Wrapper = /** @class */ (function () {
    function Wrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('rule-deserialiser')
    ], Wrapper.prototype, "ruleDeserialisers", void 0);
    return Wrapper;
}());
test("Can deserialise a rule", function () { return __awaiter(_this, void 0, void 0, function () {
    var wrapper, data, rule, _a, _b, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                wrapper = new Wrapper();
                data = {
                    name: 'test rule',
                    preCondition: {
                        type: '=',
                        name: 'preCondition',
                        left: { name: 'preCondition.left', type: 'constant', val: 'test1' },
                        right: { name: 'preCondition.right', type: 'constant', val: 'test1' }
                    },
                    trigger: {
                        type: '=',
                        name: 'trigger',
                        left: { name: 'trigger.left', type: 'constant', val: 'test2' },
                        right: { name: 'trigger.right', type: 'constant', val: 'test2' }
                    },
                    action: {
                        type: '=',
                        name: 'action',
                        left: { name: 'action.left', type: 'constant', val: 'test3' },
                        right: { name: 'action.right', type: 'constant', val: 'test4' }
                    },
                    postCondition: {
                        type: '=',
                        name: 'postCondition',
                        left: { name: 'postCondition.left', type: 'constant', val: 'test5' },
                        right: { name: 'postCondition.right', type: 'constant', val: 'test6' }
                    }
                };
                rule = wrapper.ruleDeserialisers.default(data);
                expect(rule).toBeInstanceOf(blackbox_rules_1.Rule);
                expect(rule.preCondition).toBeDefined();
                expect(rule.trigger).toBeDefined();
                expect(rule.action).toBeDefined();
                expect(rule.postCondition).toBeDefined();
                expect(rule.preCondition.checkTrue).toBeDefined();
                expect(rule.trigger.checkTrue).toBeDefined();
                expect(rule.action.checkTrue).toBeDefined();
                expect(rule.postCondition.checkTrue).toBeDefined();
                _a = expect;
                return [4 /*yield*/, rule.preCondition.checkTrue()];
            case 1:
                _a.apply(void 0, [_e.sent()]).toBeTruthy();
                _b = expect;
                return [4 /*yield*/, rule.trigger.checkTrue()];
            case 2:
                _b.apply(void 0, [_e.sent()]).toBeTruthy();
                _c = expect;
                return [4 /*yield*/, rule.action.checkTrue()];
            case 3:
                _c.apply(void 0, [_e.sent()]).toBeFalsy();
                _d = expect;
                return [4 /*yield*/, rule.postCondition.checkTrue()];
            case 4:
                _d.apply(void 0, [_e.sent()]).toBeFalsy();
                expect(rule.preCondition.left).toBeDefined();
                expect(rule.trigger.left).toBeDefined();
                expect(rule.action.left).toBeDefined();
                expect(rule.postCondition.left).toBeDefined();
                expect(rule.preCondition.right).toBeDefined();
                expect(rule.trigger.right).toBeDefined();
                expect(rule.action.right).toBeDefined();
                expect(rule.postCondition.right).toBeDefined();
                expect(rule.preCondition.left.name).toEqual(data.preCondition.left.name);
                expect(rule.trigger.left.name).toEqual(data.trigger.left.name);
                expect(rule.action.left.name).toEqual(data.action.left.name);
                expect(rule.postCondition.left.name).toEqual(data.postCondition.left.name);
                expect(rule.preCondition.right.name).toEqual(data.preCondition.right.name);
                expect(rule.trigger.right.name).toEqual(data.trigger.right.name);
                expect(rule.action.right.name).toEqual(data.action.right.name);
                expect(rule.postCondition.right.name).toEqual(data.postCondition.right.name);
                expect(rule.preCondition.left.type).toEqual(data.preCondition.left.type);
                expect(rule.trigger.left.type).toEqual(data.trigger.left.type);
                expect(rule.action.left.type).toEqual(data.action.left.type);
                expect(rule.postCondition.left.type).toEqual(data.postCondition.left.type);
                expect(rule.preCondition.right.type).toEqual(data.preCondition.right.type);
                expect(rule.trigger.right.type).toEqual(data.trigger.right.type);
                expect(rule.action.right.type).toEqual(data.action.right.type);
                expect(rule.postCondition.right.type).toEqual(data.postCondition.right.type);
                expect(rule.preCondition.left.val).toEqual(data.preCondition.left.val);
                expect(rule.trigger.left.val).toEqual(data.trigger.left.val);
                expect(rule.action.left.val).toEqual(data.action.left.val);
                expect(rule.postCondition.left.val).toEqual(data.postCondition.left.val);
                expect(rule.preCondition.right.val).toEqual(data.preCondition.right.val);
                expect(rule.trigger.right.val).toEqual(data.trigger.right.val);
                expect(rule.action.right.val).toEqual(data.action.right.val);
                expect(rule.postCondition.right.val).toEqual(data.postCondition.right.val);
                return [2 /*return*/];
        }
    });
}); });
