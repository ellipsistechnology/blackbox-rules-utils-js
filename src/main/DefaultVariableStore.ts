export default class DefaultVariableStore {
  private vars: {[key:string]:any}

  constructor() {
    this.vars = {}
  }

  put(name:string, v:any):void {
    this.vars[name] = v
  }

  get(name:string):any {
    return this.vars[name]
  }
}
