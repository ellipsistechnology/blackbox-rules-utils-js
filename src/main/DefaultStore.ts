import {Condition, Rule, Value, Store} from 'blackbox-rules'
import Database, { Data, DeleteData } from './Database';
import { autowired } from 'blackbox-ioc';

export default class DefaultStore implements Store, Data {
  conditions: {[key:string]:Condition}
  rules: {[key:string]:Rule}
  values: {[key:string]:Value<any>}

  @autowired('rules-database')
  database: Database

  private writebackDelay: number
  private maxWritebackDelay: number
  private writebackCount: number
  private previousWriteTime: number
  private putData: Data
  private deleteData: DeleteData

  constructor() {
    this.conditions = {}
    this.rules = {}
    this.values = {}

    if(this.database) {
      this.writebackDelay = 1000
      this.maxWritebackDelay = 30000
      this.writebackCount = 0
      this.previousWriteTime = 0
      this.putData = {rules: {}, conditions: {}, values: {}}
      this.deleteData = {rules: [], conditions: [], values: []}

      this.database.load(this).catch((err)=>{
        if(err)
          console.error(err)
      })
    }
  }

  writeback() {
    if(this.database) {
      this.writebackCount++
      const previousCount = this.writebackCount
      setTimeout(() => {
        const now = new Date().getMilliseconds()

        // Only write back if nothing added recently or if already waited too long:
        if(this.writebackCount === previousCount || (now - this.previousWriteTime > this.maxWritebackDelay)) {
          this.previousWriteTime = now
          this.writebackCount = 0

          const writebackData = this.putData
          this.putData = {rules: {}, conditions: {}, values: {}}
          const deleteData = this.deleteData
          this.deleteData = {rules: [], conditions: [], values: []}

          this.database.beginWriteback(this)
          deleteData.rules.forEach(name => this.database.deleteRule(name))
          deleteData.conditions.forEach(name => this.database.deleteCondition(name))
          deleteData.values.forEach(name => this.database.deleteValue(name))
          if(Object.keys(writebackData).length > 0)
            this.database.put(writebackData)
          this.database.endWriteback(this)
        }
      }, this.writebackDelay)
    }
  }

  getValue(name:string):Value<any> {
    return this.values[name]
  }

  putValue(value:Value<any>) {
    if(this.putData)  {
      this.putData.values[value.name] = value
      this.writeback()
    }
    this.values[value.name] = value
  }

  removeValue(name:string) {
    if(this.deleteData) {
      this.deleteData.values.push(name)
      this.writeback()
    }
    delete this.values[name]
  }

  getCondition(name:string) {
    return this.conditions[name]
  }

  putCondition(cond:Condition) {
    if(cond) {
      if(this.putData)  {
        this.putData.conditions[cond.name] = cond
        this.writeback()
      }
      this.conditions[cond.name] = cond
    }
  }

  removeCondition(name:string) {
    if(this.deleteData) {
      this.deleteData.conditions.push(name)
      this.writeback()
    }
    delete this.conditions[name]
  }

  getRule(name:string):Rule {
    return this.rules[name]
  }

  putRule(rule:Rule) {
    if(rule) {
      if(this.putData)  {
        this.putData.rules[rule.name] = rule
        this.writeback()
      }
      this.rules[rule.name] = rule
    }
  }

  removeRule(name:string) {
    if(this.deleteData) {
      this.deleteData.rules.push(name)
      this.writeback()
    }
    delete this.rules[name]
  }

  allConditions():Condition[] {
    return Object.values(this.conditions)
  }

  allRules():Rule[] {
    return Object.values(this.rules)
  }

  allValues():Value<any>[] {
    return Object.values(this.values)
  }
}
