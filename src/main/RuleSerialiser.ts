import { autowiredService, taggedService } from "blackbox-ioc";
import { Rule } from "blackbox-rules";

export default class RuleSerialiser {
  @autowiredService('condition-deserialiser')
  conditionDeserialisers: any

  @taggedService('rule-deserialiser', 'default')
  deserialise(data:any):Rule {
    if(!data)
      return data

    const action = this.conditionDeserialisers[data.action.type](data.action)
    const trigger = this.conditionDeserialisers[data.trigger.type](data.trigger)
    let preCondition, postCondition
    if(data.preCondition)
      preCondition = this.conditionDeserialisers[data.preCondition.type](data.preCondition)
    if(data.postCondition)
      postCondition = this.conditionDeserialisers[data.postCondition.type](data.postCondition)

    return new Rule({
      name: data.name,
      preCondition: preCondition,
      postCondition: postCondition,
      trigger: trigger,
      action: action
    })
  }

  @taggedService('rule-serialiser', 'default')
  serialise(rule:Rule):any {
    return rule
  }
}
