import RuleBase, { Store, RemoteValueProtocol, VariableStore } from 'blackbox-rules'
import DefaultStore from './DefaultStore'
import DefaultRemoteValueProtocol from './DefaultRemoteValueProtocol'
import DefaultVariableStore from './DefaultVariableStore'
import {autowired} from 'blackbox-ioc'

export default class DefaultRuleBase extends RuleBase {
  @autowired('rulebase-store')
  wiredStore: Store

  @autowired('remote-value-protocol')
  wiredRemoteValueProtocol: RemoteValueProtocol

  @autowired('variable-store')
  wiredVariableStore: VariableStore

  constructor() {
    super({
      runPeriod: 1000
    })

    if(this.wiredRemoteValueProtocol)
      this.remoteValueProtocol = this.wiredRemoteValueProtocol
    else
      this.remoteValueProtocol = new DefaultRemoteValueProtocol()

    if(this.wiredStore)
      this.store = this.wiredStore
    else
      this.store = new DefaultStore()

    if(this.wiredVariableStore)
      this.variableStore = this.wiredVariableStore
    else
      this.variableStore = new DefaultVariableStore()
  }
}
