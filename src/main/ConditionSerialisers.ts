import { autowiredService, taggedService, autowired } from "blackbox-ioc";
import RuleBase, { ConditionImpl, Condition, EqualsCondition, LessThanCondition, GreaterThanCondition, AndCondition, OrCondition } from "blackbox-rules";

/**
 * Provides serialisation and deserialisation serbices for conditions.
 * Services are tagged with condition-serialiser and condition-deserialiser.
 * Note that since conditions are also values, each serialiser is tagged with both
 * condition-serialiser and value-serialiser, and each deserialiser is tagged with
 * both condition-deserialiser and value-deserialiser
 */
export default class ConditionSerialisers {
  @autowiredService('value-deserialiser')
  valueDeserialisers: any

  @autowired('rulebase')
  rulebase:RuleBase

  deserialise(data:any, constructor:(props:any)=>Condition):Condition {
    if(!data)
      return data

    const left = this.valueDeserialisers[data.left.type](data.left)
    const right = this.valueDeserialisers[data.right.type](data.right)

    return constructor({
      name: data.name,
      left: left,
      right: right,
      type: data.type,
      template: data.template
    })
  }

  serialise(condition:Condition):any {
    return condition
  }

  @taggedService('condition-deserialiser', 'default')
  deserialiseDefault(data:any):Condition {
    return this.deserialise(data, (props:any) => new ConditionImpl(props))
  }

  @taggedService('condition-serialiser', 'default')
  serialiseDefault(condition:Condition):any {
    return this.serialise(condition)
  }

  @taggedService('condition-deserialiser', '=')
  @taggedService('value-deserialiser', '=')
  deserialiseEquals(data:any):Condition {
    return this.deserialise(data, (props:any) => new EqualsCondition(props))
  }

  @taggedService('condition-serialiser', '=')
  @taggedService('value-serialiser', '=')
  serialiseEquals(condition:Condition):any {
    return this.serialise(condition)
  }

  @taggedService('condition-deserialiser', '<')
  @taggedService('value-deserialiser', '<')
  deserialiseLessThan(data:any):Condition {
    return this.deserialise(data, (props:any) => new LessThanCondition(props))
  }

  @taggedService('condition-serialiser', '<')
  @taggedService('value-serialiser', '<')
  serialiseLessThan(condition:Condition):any {
    return this.serialise(condition)
  }

  @taggedService('condition-deserialiser', '>')
  @taggedService('value-deserialiser', '>')
  deserialiseGreaterThan(data:any):Condition {
    return this.deserialise(data, (props:any) => new GreaterThanCondition(props))
  }

  @taggedService('condition-serialiser', '>')
  @taggedService('value-serialiser', '>')
  serialiseGreaterThan(condition:Condition):any {
    return this.serialise(condition)
  }

  @taggedService('condition-deserialiser', 'and')
  @taggedService('value-deserialiser', 'and')
  deserialiseAnd(data:any):Condition {
    return this.deserialise(data, (props:any) => new AndCondition(props))
  }

  @taggedService('condition-serialiser', 'and')
  @taggedService('value-serialiser', 'and')
  serialiseAnd(condition:Condition):any {
    return this.serialise(condition)
  }

  @taggedService('condition-deserialiser', 'or')
  @taggedService('value-deserialiser', 'or')
  deserialiseOr(data:any):Condition {
    return this.deserialise(data, (props:any) => new OrCondition(props))
  }

  @taggedService('condition-serialiser', 'or')
  @taggedService('value-serialiser', 'or')
  serialiseOr(condition:Condition):any {
    return this.serialise(condition)
  }

  @taggedService('condition-deserialiser', 'unconditional')
  @taggedService('value-deserialiser', 'unconditional')
  deserialiseUnconditional(data:any):Condition {
    if(!this.rulebase)
      throw new Error(`Cannot deserialise an unconditional value without a 'rulebase' instance.`)
    const uncond = this.rulebase.unconditional()
    uncond.name = data.name
    return uncond
  }

  @taggedService('condition-serialiser', 'unconditional')
  @taggedService('value-serialiser', 'unconditional')
  serialiseUnconditional(condition:Condition):any {
    return this.serialise(condition)
  }
}
