import { Rule, Condition, Value, Store } from "blackbox-rules";

export interface Data {
  rules:{[key:string]:Rule}
  conditions:{[key:string]:Condition}
  values:{[key:string]:Value<any>}
}

export interface DeleteData {
  rules:string[]
  conditions:string[]
  values:string[]
}

export default interface Database {
  load(store: Store):Promise<void>
  save(data:Data):Promise<void>
  put(data:Data)
  deleteRule(name: string)
  deleteCondition(name: string)
  deleteValue(name: string)
  beginWriteback(data:Data)
  endWriteback(data:Data):Promise<void>
}
