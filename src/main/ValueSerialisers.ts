import { taggedService, autowired } from "blackbox-ioc";
import RuleBase, {
  Value,
  ConstantValue,
  DateTimeValue,
  LogValue,
  RemoteValue,
  RemoteValueProtocol,
  VariableValue,
  Store
} from "blackbox-rules";

export default class ValueSerialisers {
  @autowired('rulebase')
  rulebase:RuleBase

  @autowired('rulebase-store')
  store:Store

  @autowired('remote-value-protocol')
  remoteValueProtocol: RemoteValueProtocol

  @taggedService('value-deserialiser', 'default')
  deserialiseDefault(_data:any):Value<any> {
    throw new Error(`Value has no default deserialiser.`)
  }

  @taggedService('value-serialiser', 'default')
  serialiseDefault(_condition:Value<any>):any {
    throw new Error(`Value has no default serialiser.`)
  }

  @taggedService('value-deserialiser', 'constant')
  deserialiseConstant(data:any):Value<any> {
    const value = new ConstantValue(data.val);
    value.name = data.name
    return value
  }

  @taggedService('value-serialiser', 'constant')
  serialiseConstant(value:Value<any>):any {
    return value
  }

  @taggedService('value-deserialiser', 'date-time')
  deserialiseDateTime(data:any):Value<any> {
    const value = new DateTimeValue();
    value.name = data.name
    return value
  }

  @taggedService('value-serialiser', 'date-time')
  serialiseDateTime(value:Value<any>):any {
    return value
  }

  @taggedService('value-deserialiser', 'log')
  deserialiseLog(data:any):Value<any> {
    const value = new LogValue();
    value.name = data.name
    return value
  }

  @taggedService('value-serialiser', 'log')
  serialiseLog(value:Value<any>):any {
    return value
  }

  @taggedService('value-deserialiser', 'remote')
  deserialiseRemote(data:any):Value<any> {
    let remoteValueProtocol;
    if(this.remoteValueProtocol)
      remoteValueProtocol = this.remoteValueProtocol
    else if(this.rulebase && this.rulebase.remoteValueProtocol)
      remoteValueProtocol = this.rulebase.remoteValueProtocol
    if(!remoteValueProtocol)
      throw new Error(
        'Cannot deserialise remote value without remote value protocol: '+
        'Remote value protocol not found in either \'remote-value-protocol\' instance'+
        'or in \'rulebase\' instance'
      )

    const value = new RemoteValue(remoteValueProtocol)
    value.name = data.name
    value.remoteValueName = data.remoteValueName
    value.remoteAddress = data.remoteAddress
    return value
  }

  @taggedService('value-serialiser', 'remote')
  serialiseRemote(value:RemoteValue<any>):any {
    return {
      remoteAddress: value.remoteAddress,
      remoteValueName: value.remoteValueName,
      name: value.name,
      type: value.type
    }
  }

  @taggedService('value-deserialiser', 'variable')
  deserialiseVariable(data:any):Value<any> {
    let store;
    if(this.store)
      store = this.store
    else if(this.rulebase && this.rulebase.store)
      store = this.rulebase.store
    if(!store)
      throw new Error(
        'Cannot deserialise variable value without store: '+
        'Store not found in either \'rulebase-store\' instance'+
        'or in \'rulebase\' instance'
      )

    const value = new VariableValue(store);
    value.name = data.name
    value.variableName = data.variableName
    return value
  }

  @taggedService('variable-serialiser', 'variable')
  serialiseVariable(value:Value<any>):any {
    return value
  }
}
