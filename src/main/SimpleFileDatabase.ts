import Database, { Data } from "./Database";
import { Store } from "blackbox-rules";
import { autowiredService } from "blackbox-ioc";
const fs = require('fs');

export default class SimpleFileDatabase implements Database {
  path:string

  @autowiredService('rule-deserialiser')
  ruleDeserialisers: any

  @autowiredService('condition-deserialiser')
  conditionDeserialisers: any

  @autowiredService('value-deserialiser')
  valueDeserialisers: any

  @autowiredService('rule-serialiser')
  ruleSerialisers: any

  @autowiredService('condition-serialiser')
  conditionSerialisers: any

  @autowiredService('value-serialiser')
  valueSerialisers: any

  constructor({path}) {
    this.path = path
  }

  save(data: any):Promise<void> {
    const db:any = {rules:{}, conditions:{}, values:{}}
    Object.values(data.rules).forEach((rule:any) => db.rules[rule.name] = this.ruleSerialisers.default(rule))
    Object.values(data.conditions).forEach((cond:any) => db.conditions[cond.name] = this.conditionSerialisers[cond.type](cond))
    Object.values(data.values).forEach((val:any) => db.values[val.name] = this.valueSerialisers[val.type](val))
    return new Promise((resolve, reject) => {
      fs.writeFile(this.path, JSON.stringify(db), (err) => {
        if(err)
          reject(err)
        else
          resolve()
      })
    })
  }

  load(store: Store):Promise<void> {
    return new Promise((resolve, reject) => {
      if(!fs.existsSync(this.path))
        fs.writeFileSync(this.path, `{"rules":{}, "conditions":{}, "values":{}}`)
      fs.readFile(this.path, (err, dataStr) => {
        if(!err && !dataStr)
          err = new Error(`Failed to load data from path '${this.path}'`)
        if(!err) {
          const data = JSON.parse(dataStr)
          Object.values(data.rules).forEach(rule => store.putRule(this.ruleDeserialisers.default(rule)))
          Object.values(data.conditions).forEach((cond:any) => store.putCondition(this.conditionDeserialisers[cond.type](cond)))
          Object.values(data.values).forEach((val:any) => store.putValue(this.valueDeserialisers[val.type](val)))
          resolve()
        }
        else {
          reject(err)
        }
      })
    })
  }

  beginWriteback(data: Data) {
      // Does nothing - relies on endWriteback to write the whole file in one go.
  }

  endWriteback(data: Data):Promise<void> {
    return this.save(data)
  }

  deleteRule(name: string) {
    // Does nothing - relies on endWriteback to write the whole file in one go.
  }

  deleteCondition(name: string) {
    // Does nothing - relies on endWriteback to write the whole file in one go.
  }

  deleteValue(name: string) {
    // Does nothing - relies on endWriteback to write the whole file in one go.
  }

  put(data: Data) {
    // Does nothing - relies on endWriteback to write the whole file in one go.
  }
}
