import {RemoteValueProtocol} from 'blackbox-rules'
import axios from 'axios'

export default class DefaultRemoteValueProtocol implements RemoteValueProtocol {
  lookupAgentValueAddress(agentAddress:string): string {
    return agentAddress+"/values"
  }

  setValue<T>(agentValueAddress:string, remoteValueName:string, value:T): Promise<T> {
    return axios.patch(`${agentValueAddress}/${remoteValueName}`, {value:value})
  }

  getValue<T>(agentValueAddress:string, remoteValueName:string): Promise<T> {
    return axios.get(`${agentValueAddress}/${remoteValueName}`)
  }
}
