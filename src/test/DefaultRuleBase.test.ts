import DefaultRuleBase from '../main/DefaultRuleBase'

test("Can create DefaultRuleBase", async () => {
  const rb = new DefaultRuleBase()
  expect(rb.remoteValueProtocol).toBeDefined()

  expect(rb.remoteValueProtocol.lookupAgentValueAddress).toBeInstanceOf(Function)
  expect(rb.remoteValueProtocol.getValue).toBeInstanceOf(Function)
  expect(rb.remoteValueProtocol.setValue).toBeInstanceOf(Function)

  expect(rb.variableStore).toBeDefined()
  expect(rb.variableStore.get).toBeInstanceOf(Function)
  expect(rb.variableStore.put).toBeInstanceOf(Function)

  expect(rb.store).toBeDefined()
  expect(rb.store.putCondition).toBeInstanceOf(Function)
  expect(rb.store.getCondition).toBeInstanceOf(Function)
  expect(rb.store.removeCondition).toBeInstanceOf(Function)
  expect(rb.store.allConditions).toBeInstanceOf(Function)
  expect(rb.store.getRule).toBeInstanceOf(Function)
  expect(rb.store.putRule).toBeInstanceOf(Function)
  expect(rb.store.removeRule).toBeInstanceOf(Function)
  expect(rb.store.allRules).toBeInstanceOf(Function)
  expect(rb.store.getValue).toBeInstanceOf(Function)
  expect(rb.store.putValue).toBeInstanceOf(Function)
  expect(rb.store.removeValue).toBeInstanceOf(Function)
  expect(rb.store.allValues).toBeInstanceOf(Function)

  expect(rb.unconditional).toBeDefined()
  expect(await rb.unconditional().checkTrue()).toBe(true)
})
