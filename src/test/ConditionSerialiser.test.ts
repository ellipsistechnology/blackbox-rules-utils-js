import { autowiredService, factory } from "blackbox-ioc";
import { ConditionImpl, ConstantValue, EqualsCondition, LessThanCondition, GreaterThanCondition } from "blackbox-rules";
import ConditionSerialisers from '../main/ConditionSerialisers'
import ValueSerialisers from '../main/ValueSerialisers'
import DefaultRuleBase from "../main/DefaultRuleBase";

// Force loading of services:
new ConditionSerialisers()
new ValueSerialisers()

class Wrapper {
  @autowiredService('condition-serialiser')
  serialisers: any

  @autowiredService('condition-deserialiser')
  deserialisers: any
  
  @factory('rulebase')
  rulebase() { return new DefaultRuleBase() }
}

test("Can deserialise a default condition", () => {
  const wrapper = new Wrapper()
  const data = {
    name: "test condition",
    left: {
      name: 'test',
      type: 'constant',
      val: { thing: 'test value'}
    },
    right: {
      name: 'test',
      type: 'constant',
      val: { thing: 'test value'}
    },
    type: "x",
    template: {
      name: "test template"
    }
  }

  const deserialised = wrapper.deserialisers['default'](data)
  expect(deserialised).toBeInstanceOf(ConditionImpl)
  expect(deserialised.left).toBeInstanceOf(ConstantValue)
  expect(deserialised.right).toBeInstanceOf(ConstantValue)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(deserialised.template).toEqual(data.template)
})

function testCondition(type:string, Type:Function) {
  const wrapper = new Wrapper()
  const data = {
    name: "test condition",
    left: {
      name: 'test',
      type: 'constant',
      val: { thing: 'test value'}
    },
    right: {
      name: 'test',
      type: 'constant',
      val: { thing: 'test value'}
    },
    type: type,
    template: {
      name: "test template"
    }
  }

  const deserialised = wrapper.deserialisers[type](data)
  expect(deserialised).toBeInstanceOf(Type)
  expect(deserialised.left).toBeInstanceOf(ConstantValue)
  expect(deserialised.right).toBeInstanceOf(ConstantValue)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(deserialised.template).toEqual(data.template)
  expect((deserialised as ConditionImpl).checkTrue()).toBeTruthy()
}

test("Can deserialise an equals condition", () => {
  testCondition("=", EqualsCondition)
})

test("Can deserialise a less then condition", () => {
  testCondition("<", LessThanCondition)
})

test("Can deserialise a greater than condition", () => {
  testCondition(">", GreaterThanCondition)
})

function testAndOr(type:string) {
  const wrapper = new Wrapper()
  const data = {
    name: "test condition",
    left: {
      name: "left",
      type: "=",
      left: {
        name: 'test',
        type: 'constant',
        val: { thing: 'test value'}
      },
      right: {
        name: 'test',
        type: 'constant',
        val: { thing: 'test value'}
      }
    },
    right: {
      name: "right",
      type: "=",
      left: {
        name: 'test',
        type: 'constant',
        val: { thing: 'test value'}
      },
      right: {
        name: 'test',
        type: 'constant',
        val: { thing: 'test value'}
      }
    },
    type: type,
    template: {
      name: "test template"
    }
  }

  const deserialised = wrapper.deserialisers[type](data)
  expect(deserialised).toBeInstanceOf(ConditionImpl)
  expect(deserialised.left).toBeInstanceOf(ConditionImpl)
  expect(deserialised.left.name).toEqual(data.left.name)
  expect(deserialised.right).toBeInstanceOf(ConditionImpl)
  expect(deserialised.right.name).toEqual(data.right.name)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(deserialised.template).toEqual(data.template)
  expect((deserialised as ConditionImpl).checkTrue()).toBeTruthy()
}

test("Can deserialise an and condition", () => {
  testAndOr("and")
})

test("Can deserialise an or condition", () => {
  testAndOr("or")
})

test("Can deserialise an unconditional", () => {
  const wrapper = new Wrapper()
  const data = {
    name: "test unconditional",
    type: "unconditional"
  }

  const deserialised = wrapper.deserialisers.unconditional(data)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(deserialised.checkTrue()).toBeTruthy()
})
