import { autowiredService, factory } from "blackbox-ioc";
import { ConstantValue, DateTimeValue, LogValue, RemoteValue, VariableValue } from "blackbox-rules";
import ValueSerialisers from '../main/ValueSerialisers'
import { DefaultRuleBase } from "..";

new ValueSerialisers() // cause the decorators to be called

class Wrapper {
  @autowiredService('value-deserialiser')
  valueDeserialisers: any

  @autowiredService('value-serialiser')
  valueSerialisers: any

  @factory('rulebase')
  rulebase() { return new DefaultRuleBase() }
}

test("Can deserialise a constant value", async () => {
  const wrapper = new Wrapper()
  const data = {
    name: 'test',
    type: 'constant',
    val: { thing: 'test value'}
  }
  const deserialised = wrapper.valueDeserialisers.constant(data)

  expect(deserialised).toBeInstanceOf(ConstantValue)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(await (deserialised as ConstantValue<any>).get()).toEqual(data.val)
})

test("Can deserialise a date-time value", () => {
  const wrapper = new Wrapper()
  const data = {
    name: 'test',
    type: 'date-time'
  }
  const deserialised = wrapper.valueDeserialisers['date-time'](data)

  expect(deserialised).toBeInstanceOf(DateTimeValue)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
})

test("Can deserialise a remote value", () => {
  const wrapper = new Wrapper()
  const data = {
    name: 'test',
    type: 'remote',
    remoteAddress: 'http://example.com/rulebase',
    remoteValueName: 'remoteTest'
  }
  const deserialised = wrapper.valueDeserialisers.remote(data)

  expect(deserialised).toBeInstanceOf(RemoteValue)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(deserialised.remoteAddress).toEqual(data.remoteAddress)
  expect(deserialised.remoteValueName).toEqual(data.remoteValueName)
})

test("Can deserialise a variable value", () => {
  const wrapper = new Wrapper()
  const data = {
    name: 'test',
    type: 'variable',
    variableName: 'test name'
  }
  const deserialised = wrapper.valueDeserialisers['variable'](data)

  expect(deserialised).toBeInstanceOf(VariableValue)
  expect(deserialised.name).toEqual(data.name)
  expect(deserialised.type).toEqual(data.type)
  expect(deserialised.variableName).toEqual(data.variableName)
})
