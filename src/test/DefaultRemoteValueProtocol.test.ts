import DefaultRemoteValueProtocol from '../main/DefaultRemoteValueProtocol'

import axios from 'axios'

(axios as any).__setup__({
  'patch': {'test':'patched'},
  'get': {'test':'got'}
})

// TODO Fix these tests... the below tests don't really test basic
//      functionality and require assumptions about the use of axios
test("Can create DefaultRemoteValueProtocol", async () => {
  const rvp = new DefaultRemoteValueProtocol()
  expect(rvp.lookupAgentValueAddress('agent')).toEqual('agent/values')
  await rvp.setValue('agent/values', 'test', 'abc')
  expect(axios.patch).toHaveBeenCalledWith('agent/values/test', {value:'abc'})
  expect(await rvp.getValue('agent/values', 'test')).toEqual({'test':'got'})
  expect(axios.get).toHaveBeenCalledWith('agent/values/test')
})
