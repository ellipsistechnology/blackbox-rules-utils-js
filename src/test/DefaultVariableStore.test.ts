import DefaultVariableStore from '../main/DefaultVariableStore'

const TEST_NAME1 = 'test 1'
const TEST_NAME2 = 'test 2'
const TEST_NAME3 = 'test 3'
const TEST_VALUE1 = 'string'
const TEST_VALUE2 = 1
const TEST_VALUE3 = {thing:'string'}

test("Can create DefaultVariableStore", () => {
  let store = new DefaultVariableStore()
  store.put(TEST_NAME1, TEST_VALUE1)
  store.put(TEST_NAME2, TEST_VALUE2)
  store.put(TEST_NAME3, TEST_VALUE3)
  expect(store.get(TEST_NAME1)).toBe(TEST_VALUE1)
  expect(store.get(TEST_NAME2)).toBe(TEST_VALUE2)
  expect(store.get(TEST_NAME3)).toBe(TEST_VALUE3)
})
