import DefaultStore from '../main/DefaultStore'
import {Rule, Condition, Value} from 'blackbox-rules'
import { factory } from 'blackbox-ioc';
import SimpleFileDatabase from '../main/SimpleFileDatabase';

jest.mock('fs');

const RULE_NAME = "Rule 001"
const CONDITION_NAME = "Condition 001"
const VALUE_NAME = "Value 001"

test("Can create DefaultStore", () => {
  const store = new DefaultStore()
  expect(store.allRules).toBeDefined()
  expect(store.allRules.length).toBe(0)
  expect(store.allConditions).toBeDefined()
  expect(store.allConditions.length).toBe(0)
  expect(store.allValues).toBeDefined()
  expect(store.allValues.length).toBe(0)
})

function testRules() {
  const store = new DefaultStore()
  const r = new Rule({name:RULE_NAME})
  store.putRule(r)
  expect(store.getRule(RULE_NAME)).toBe(r)
  store.removeRule(RULE_NAME)
  expect(store.getRule(RULE_NAME)).toBeUndefined()
}

test("DefaultStore can store, load and delete rules", testRules)

function testConditions() {
  const store = new DefaultStore()
  const c = {
    name: CONDITION_NAME,
    type: 'test',
    checkTrue: async () => true,
    makeTrue: async () => {},
    get: async () => null,
    set: (v) => {}
  }
  store.putCondition(c)
  expect(store.getCondition(CONDITION_NAME)).toBe(c)
  store.removeCondition(CONDITION_NAME)
  expect(store.getCondition(CONDITION_NAME)).toBeUndefined()
}

test("DefaultStore can store, load and delete conditions", testConditions)

function testValues() {
  const store = new DefaultStore()
  const v = {
    name: VALUE_NAME,
    type: 'test',
    get: async () => null,
    set: (v) => {}
  }
  store.putValue(v)
  expect(store.getValue(VALUE_NAME)).toBe(v)
  store.removeValue(VALUE_NAME)
  expect(store.getValue(VALUE_NAME)).toBeUndefined()
}

test("DefaultStore can store, load and delete values", testValues)

test("DefaultStore can create, get and delete rules, conditions and values with a database", () => {
  class Factory {
    @factory('rules-database')
    makeDatabase() { return new SimpleFileDatabase({path: '/'}); }
  }

  const store = new DefaultStore()
  expect(store.database).toBeDefined()

  testRules()
  testConditions()
  testValues()
})
