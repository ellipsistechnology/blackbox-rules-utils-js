import { autowiredService } from "blackbox-ioc";
import { Rule } from "blackbox-rules";
import RuleSerialiser from "../main/RuleSerialiser";
import ValueSerialisers from "../main/ValueSerialisers";
import ConditionSerialisers from "../main/ConditionSerialisers";

new RuleSerialiser()
new ConditionSerialisers()
new ValueSerialisers()

class Wrapper {
  @autowiredService('rule-deserialiser')
  ruleDeserialisers: any
}

test("Can deserialise a rule", async () => {
  const wrapper = new Wrapper()
  const data = {
    name: 'test rule',
    preCondition: {
      type: '=',
      name: 'preCondition',
      left: {name:'preCondition.left', type: 'constant', val: 'test1'},
      right: {name:'preCondition.right', type: 'constant', val: 'test1'}
    },
    trigger: {
      type: '=',
      name: 'trigger',
      left: {name:'trigger.left', type: 'constant', val: 'test2'},
      right: {name:'trigger.right', type: 'constant', val: 'test2'}
    },
    action: {
      type: '=',
      name: 'action',
      left: {name:'action.left', type: 'constant', val: 'test3'},
      right: {name:'action.right', type: 'constant', val: 'test4'}
    },
    postCondition: {
      type: '=',
      name: 'postCondition',
      left: {name:'postCondition.left', type: 'constant', val: 'test5'},
      right: {name:'postCondition.right', type: 'constant', val: 'test6'}
    }
  }

  const rule = wrapper.ruleDeserialisers.default(data)
  expect(rule).toBeInstanceOf(Rule)

  expect(rule.preCondition).toBeDefined()
  expect(rule.trigger).toBeDefined()
  expect(rule.action).toBeDefined()
  expect(rule.postCondition).toBeDefined()

  expect(rule.preCondition.checkTrue).toBeDefined()
  expect(rule.trigger.checkTrue).toBeDefined()
  expect(rule.action.checkTrue).toBeDefined()
  expect(rule.postCondition.checkTrue).toBeDefined()

  expect(await rule.preCondition.checkTrue()).toBeTruthy()
  expect(await rule.trigger.checkTrue()).toBeTruthy()
  expect(await rule.action.checkTrue()).toBeFalsy()
  expect(await rule.postCondition.checkTrue()).toBeFalsy()

  expect(rule.preCondition.left).toBeDefined()
  expect(rule.trigger.left).toBeDefined()
  expect(rule.action.left).toBeDefined()
  expect(rule.postCondition.left).toBeDefined()

  expect(rule.preCondition.right).toBeDefined()
  expect(rule.trigger.right).toBeDefined()
  expect(rule.action.right).toBeDefined()
  expect(rule.postCondition.right).toBeDefined()

  expect(rule.preCondition.left.name).toEqual(data.preCondition.left.name)
  expect(rule.trigger.left.name).toEqual(data.trigger.left.name)
  expect(rule.action.left.name).toEqual(data.action.left.name)
  expect(rule.postCondition.left.name).toEqual(data.postCondition.left.name)

  expect(rule.preCondition.right.name).toEqual(data.preCondition.right.name)
  expect(rule.trigger.right.name).toEqual(data.trigger.right.name)
  expect(rule.action.right.name).toEqual(data.action.right.name)
  expect(rule.postCondition.right.name).toEqual(data.postCondition.right.name)

  expect(rule.preCondition.left.type).toEqual(data.preCondition.left.type)
  expect(rule.trigger.left.type).toEqual(data.trigger.left.type)
  expect(rule.action.left.type).toEqual(data.action.left.type)
  expect(rule.postCondition.left.type).toEqual(data.postCondition.left.type)

  expect(rule.preCondition.right.type).toEqual(data.preCondition.right.type)
  expect(rule.trigger.right.type).toEqual(data.trigger.right.type)
  expect(rule.action.right.type).toEqual(data.action.right.type)
  expect(rule.postCondition.right.type).toEqual(data.postCondition.right.type)

  expect(rule.preCondition.left.val).toEqual(data.preCondition.left.val)
  expect(rule.trigger.left.val).toEqual(data.trigger.left.val)
  expect(rule.action.left.val).toEqual(data.action.left.val)
  expect(rule.postCondition.left.val).toEqual(data.postCondition.left.val)

  expect(rule.preCondition.right.val).toEqual(data.preCondition.right.val)
  expect(rule.trigger.right.val).toEqual(data.trigger.right.val)
  expect(rule.action.right.val).toEqual(data.action.right.val)
  expect(rule.postCondition.right.val).toEqual(data.postCondition.right.val)
})
