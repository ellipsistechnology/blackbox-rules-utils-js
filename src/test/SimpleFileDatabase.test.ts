import SimpleFileDatabase from "../main/SimpleFileDatabase";
import { Rule, Value, Condition } from "blackbox-rules";
import DefaultStore from "../main/DefaultStore";
import RuleSerialiser from "../main/RuleSerialiser";
import ConditionSerialisers from "../main/ConditionSerialisers";
import ValueSerialisers from "../main/ValueSerialisers";

new RuleSerialiser()
new ConditionSerialisers()
new ValueSerialisers()

jest.mock('fs');
const fs = require('fs');

function makeTestData() {
  return {
    rules: {
      'test rule': {
        name: 'test rule',
        preCondition: {
          type: '=',
          name: 'preCondition',
          left: {name:'preCondition.left', type: 'constant', val: 'test1'},
          right: {name:'preCondition.right', type: 'constant', val: 'test1'}
        },
        trigger: {
          type: '=',
          name: 'trigger',
          left: {name:'trigger.left', type: 'constant', val: 'test2'},
          right: {name:'trigger.right', type: 'constant', val: 'test2'}
        },
        action: {
          type: '=',
          name: 'action',
          left: {name:'action.left', type: 'constant', val: 'test3'},
          right: {name:'action.right', type: 'constant', val: 'test4'}
        },
        postCondition: {
          type: '=',
          name: 'postCondition',
          left: {name:'postCondition.left', type: 'constant', val: 'test5'},
          right: {name:'postCondition.right', type: 'constant', val: 'test6'}
        }
      }
    },
    conditions: {
      preCondition: {
        type: '=',
        name: 'preCondition',
        left: {name:'preCondition.left', type: 'constant', val: 'test1'},
        right: {name:'preCondition.right', type: 'constant', val: 'test1'}
      },
      trigger: {
        type: '=',
        name: 'trigger',
        left: {name:'trigger.left', type: 'constant', val: 'test2'},
        right: {name:'trigger.right', type: 'constant', val: 'test2'}
      },
      action: {
        type: '=',
        name: 'action',
        left: {name:'action.left', type: 'constant', val: 'test3'},
        right: {name:'action.right', type: 'constant', val: 'test4'}
      },
      postCondition: {
        type: '=',
        name: 'postCondition',
        left: {name:'postCondition.left', type: 'constant', val: 'test5'},
        right: {name:'postCondition.right', type: 'constant', val: 'test6'}
      }
    },
    values: {
      'preCondition.left': {name:'preCondition.left', type: 'constant', val: 'test1'},
      'preCondition.right': {name:'preCondition.right', type: 'constant', val: 'test1'},
      'trigger.left': {name:'trigger.left', type: 'constant', val: 'test2'},
      'trigger.right': {name:'trigger.right', type: 'constant', val: 'test2'},
      'action.left': {name:'action.left', type: 'constant', val: 'test3'},
      'action.right': {name:'action.right', type: 'constant', val: 'test4'},
      'postCondition.left': {name:'postCondition.left', type: 'constant', val: 'test5'},
      'postCondition.right': {name:'postCondition.right', type: 'constant', val: 'test6'}
    }
  }
}

test("Can save and load data", async () => {
  const database = new SimpleFileDatabase({path:'/test.json'})
  const store = new DefaultStore()
  const testData = makeTestData()

  await database.save(testData)
  await database.load(store)

  expect(store.getRule('test rule')).toEqual(testData.rules['test rule'])
  expect(store.getCondition('action')).toEqual(testData.conditions['action'])
  expect(store.getValue('trigger.right')).toEqual(testData.values['trigger.right'])
})

test("Can add rules, conditions and values in a writeback transaction.", async () => {

  const database = new SimpleFileDatabase({path:'/test.json'})
  const store = new DefaultStore()
  const testData = makeTestData()

  await database.save(testData)

  database.beginWriteback(store)
  store.putRule(<Rule><any>{
    name: 'test rule 2',
    preCondition: {
      type: '=',
      name: 'preCondition',
      left: {name:'preCondition.left', type: 'constant', val: 'test1'},
      right: {name:'preCondition.right', type: 'constant', val: 'test1'}
    },
    trigger: {
      type: '=',
      name: 'trigger',
      left: {name:'trigger.left', type: 'constant', val: 'test2'},
      right: {name:'trigger.right', type: 'constant', val: 'test2'}
    },
    action: {
      type: '=',
      name: 'action',
      left: {name:'action.left', type: 'constant', val: 'test3'},
      right: {name:'action.right', type: 'constant', val: 'test4'}
    },
    postCondition: {
      type: '=',
      name: 'postCondition',
      left: {name:'postCondition.left', type: 'constant', val: 'test5'},
      right: {name:'postCondition.right', type: 'constant', val: 'test6'}
    }
  })
  store.putCondition(<Condition><any>{
    type: '=',
    name: 'preCondition2',
    left: {name:'preCondition.left', type: 'constant', val: 'test1'},
    right: {name:'preCondition.right', type: 'constant', val: 'test1'}
  })
  store.putValue(<Value<any>><any>{name:'preCondition.right2', type: 'constant', val: 'test1'})
  await database.endWriteback(store)

  const data = JSON.parse(fs.__fileMap()['/test.json'])

  expect(data.rules['test rule 2']).toBeDefined()
  expect(data.conditions['preCondition2']).toBeDefined()
  expect(data.values['preCondition.right2']).toBeDefined()
})

test("Can delete rules, conditions and values in a writeback transaction.", async () => {

  const database = new SimpleFileDatabase({path:'/test.json'})
  const store = new DefaultStore()
  const testData = makeTestData()

  await database.save(testData)

  database.beginWriteback(store)

  store.removeRule('testRule')
  store.removeCondition('testCondition')
  store.removeValue('testValue')

  database.deleteRule('testRule')
  database.deleteCondition('testCondition')
  database.deleteValue('testValue')

  await database.endWriteback(store)

  const data = JSON.parse(fs.__fileMap()['/test.json'])
  expect(data.rules.testRule).toBeUndefined()
  expect(data.conditions.testCondition).toBeUndefined()
  expect(data.values.testValue).toBeUndefined()
})
