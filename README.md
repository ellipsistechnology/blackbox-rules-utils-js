# blackbox-rules-utils

[![Version](https://img.shields.io/bitbucket/pipelines/ellipsistechnology/blackbox-rules-utils-js.svg)](https://bitbucket.org/ellipsistechnology/blackbox-rules-utils-js/addon/pipelines/home#!/)
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg)](https://github.com/RichardLitt/standard-readme)

> Blackbox rules utility classes and default implementations.

This package provides a default implementation of a `RuleBase` that includes all required properties for managing and persisting `Rules`, `Conditions` and `Values`.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
npm i blackbox-rules-utils
```

## Usage

Create a rulebase with custom Store, RemoteValueProtocol and VariableStore:
```
import {factory} from 'blackbox-ioc'

class MyRulebaseConfig {
  @factory('rulebase-store')
  createStore() { return new MyStore(); }

  @factory('remote-value-protocol')
  createRemoteValueProtocol() { return new MyRemoteValueProtocol(); }

  @factory('variable-store')
  createVariableStore() { return new MyVariableStore(); }
}

const rulebase = new DefaultRuleBase()
```
If any of the instances (created above with the factory decorator) are not provided then the defaults are used: `DefaultStore`, `DefaultRemoteValueProtocol`, and `DefaultVariableStore`.

If you are using a `DefaultStore` you can specify the database to use by providing an instance named `rules-database`, for example, via a factory decorator:
```
@factory('rules-database')
createDatabase() {
  return new SimpleFileDatabase({path:'/path/to/rulebase.json'})
}
```
or via a named class:
```
@named('rules-database')
class MyDB implements Database {
  ...
}
```

If using a `SimpleFileDatabase` then serialisers and deserialisers are needed for `Rules`, `Conditions` and `Values`. Deserialisers are obtained from serialiser and deserialiser services with the following tags: `rule-deserialiser`
`condition-deserialiser`
`value-deserialiser`
`rule-serialiser`
`condition-serialiser`
`value-serialiser`.
The `RuleSerialiser`, `ConditionSerialiser` and `ValueSerialiser` classes provide these services automatically for `Rule`s, and all `Condition`s and `Value`s provided by the blackbox-rules package.

To provide serialisers and deserialisers for custom `Condition`s and `Value`s you will need to create `taggedService`s with the service name matching your `Condition` or `Value` `type`:
```
@taggedService('condition-deserialiser', 'my-type')
deserialise(data:any):MyCondition {
  ...
}

@taggedService('condition-serialiser', 'my-type')
serialise(data:MyCondition):any {
  ...
}
```

## API

**DefaultRuleBase**: An implementation of a `RuleBase` that allows for overriding of its properties via named instances in the [Blackbox IOC](https://www.npmjs.com/package/blackbox-ioc) container (see [Usage](#Usage) above).

**DefaultStore**: A `Store` potentially backed by a `Database`. See [Usage](#Usage) above for examples of use and overrides.

**DefaultRemoteValueProtocol**: A basic `RemoteValueProtocol` that handles calling a remote Blackbox rulebase service.

**DefaultVariableStore**: A very basic in memory `VariableStore`.

**Database**: An interface for a database that persists the rulebase managed by a `DefaultStore`.

**SimpleFileDatabase**: A `Database` that persists data, via serialisers and deserialisers, to a file (see [Usage](#Usage) above).

**RuleSerialiser**: Default JSON serialisers and deserialisers for `Rule`s.

**ConditionSerialisers**: Default JSON serialisers and deserialisers for `Condition`s.

**ValueSerialisers**: Default JSON serialisers and deserialisers for `Value`s.


## Maintainers

[@ellipsistechnology](https://github.com/ellipsistechnology)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Ben Millar
