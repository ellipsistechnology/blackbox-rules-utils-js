let testData = {};

module.exports.default = {
  get: jest.fn(() => Promise.resolve(testData['get'])),
  patch: jest.fn(() => Promise.resolve(testData['post'])),
  __setup__: (data) => testData = data
};
