
const fs = jest.genMockFromModule('fs');

let fileMap = {};

function writeFile(path, data, callback) {
  fileMap[path] = data;
  callback(null);
}

function writeFileSync(path, data) {
  fileMap[path] = data;
}

function readFile(path, callback) {
  if(!fileMap[path])
    throw new Error(`File does not exist at path '${path}'`);
  callback(null, fileMap[path]);
}

function readFileSync(path) {
  if(!fileMap[path])
    throw new Error(`File does not exist at path '${path}'`);
  return fileMap[path];
}

function existsSync(path) {
  return fileMap[path] !== undefined
}

function __fileMap() {
  return fileMap;
}

fs.writeFile = writeFile;
fs.readFile = readFile;
fs.existsSync = existsSync;
fs.writeFileSync = writeFileSync;
fs.readFileSync = readFileSync;
fs.__fileMap = __fileMap;

module.exports = fs;
